const {writeFile} = require('../get-fs')().promises;
const QuasarConfFile = require('../lib/QuasarConfFile');
const I18nBootFile = require('../lib/I18nBootFile');
const I18nIndexFile = require('../lib/I18nIndexFile');
const InstallPackageCommand = require('../lib/InstallPackageCommand');

class I18nChangesCommand {
    constructor (projectPath, dryRun, install, useYarn, useNpm) {
        this.projectPath = projectPath;
        this.quasarConfFile = new QuasarConfFile(this.projectPath);
        this.i18nBootFile = new I18nBootFile(this.projectPath);
        this.i18nIndexFile = new I18nIndexFile(this.projectPath);
        this.dryRun = !!dryRun;
        this.install = install;
        this.useYarn = useYarn;
        this.useNpm = useNpm;
    }

    async execute () {
        this.quasarConfFile.open();
        this.quasarConfFile.replaceOldLocales();

        this.i18nBootFile.open();
        this.i18nBootFile.removeVueUse();
        this.i18nBootFile.addImports();
        this.i18nBootFile.replaceInstantiation();
        this.i18nBootFile.replaceOldLocales();
        this.i18nBootFile.useAppUse();

        this.i18nIndexFile.open();
        this.i18nIndexFile.replaceOldLocales();

        const quasarConf = this.quasarConfFile.save();
        const i18nBootFile = this.i18nBootFile.save();
        const i18nIndexFile = this.i18nIndexFile.save();

        if (this.dryRun) {
            console.log(quasarConf);
            console.log(i18nBootFile);
            console.log(i18nIndexFile);
        } else {
            await writeFile(this.quasarConfFile.filePath, quasarConf, 'utf8');
            await writeFile(this.i18nBootFile.filePath, i18nBootFile, 'utf8');
            await writeFile(this.i18nIndexFile.filePath, i18nIndexFile, 'utf8');
        }

        if (this.install) {
            console.log('installing vue-i18n@rc in project', this.projectPath);
            return this.executeInstall('vue-i18n@rc');
        }
    }

    executeInstall (packageName, dev = false) {
        const installPackageCommand = new InstallPackageCommand(this.projectPath, this.useYarn, this.useNpm);
        return installPackageCommand.execute(packageName, dev);
    }
}

module.exports = I18nChangesCommand;
