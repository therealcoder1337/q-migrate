const {join} = require('path');
const getFilesRecursive = require('../helpers/get-files-recursive');
const colors = require('colors/safe');

class StylusToScssCommand {
    constructor (projectPath) {
        this.projectPath = projectPath;
    }

    async execute () {
        const files = await getFilesRecursive(join(this.projectPath, 'src', 'css'));
        const stylusFiles = files.filter(entry => entry.endsWith('.styl'));

        if (stylusFiles.length) {
            stylusFiles.forEach(stylusFile => {
                console.log(colors.red(`found stylus file, which you need to convert to .scss or .sass: ${stylusFile}`));
            });
        }
    }
}

module.exports = StylusToScssCommand;
