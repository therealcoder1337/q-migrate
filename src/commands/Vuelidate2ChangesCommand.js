const VuelidateBootFile = require('../lib/VuelidateBootFile');
const VuelidateFile = require('../lib/VuelidateFile');
const InstallPackageCommand = require('../lib/InstallPackageCommand');
const getFilesRecursive = require('../helpers/get-files-recursive');
const {join, relative} = require('path');
const {writeFile} = require('fs').promises;

class Vuelidate2ChangesCommand {
    constructor (projectPath, dryRun, useSetup, dollar, install, useYarn, useNpm) {
        this.projectPath = projectPath;
        this.dryRun = !!dryRun;
        this.useSetup = !!useSetup;
        this.dollar = !!dollar;
        this.install = install;
        this.useYarn = useYarn;
        this.useNpm = useNpm;
    }

    async execute () {
        this.processBootFile();

        const vueFilePaths = await this.getAllVueFilePaths();

        for (const vueFilePath of vueFilePaths) {
            const relativePath = relative(this.projectPath, vueFilePath);
            const vuelidateFile = new VuelidateFile(this.projectPath, relativePath);

            vuelidateFile.open();

            if (!vuelidateFile.isVuelidateFile()) {
                continue;
            }

            console.log('process vuelidate file', relativePath);

            if (this.useSetup) {
                vuelidateFile.ensureDefaultImport('useVuelidate', '@vuelidate/core');
                vuelidateFile.addVuelidateSetup();
            }

            vuelidateFile.rewriteValidatorImports();

            if (this.dollar) {
                console.log('replacing $v with v$...');
                vuelidateFile.replaceDollar();
            }

            const vuelidateFileString = vuelidateFile.save();

            if (this.dryRun) {
                console.log(vuelidateFileString);
            } else {
                const savePath = vuelidateFile.filePath;
                if (!savePath || !savePath.endsWith(relativePath)) {
                    console.error('unexpected file path, saving aborted:', savePath);
                    process.exit(1);
                }
                await writeFile(savePath, vuelidateFileString, 'utf8');
            }
        }

        if (this.install) {
            console.log('installing @vuelidate/core and @vuelidate/validators in project', this.projectPath);
            await this.executeInstall('@vuelidate/core');
            return this.executeInstall('@vuelidate/validators');
        }
    }

    async processBootFile () {
        const vuelidateBootFile = new VuelidateBootFile(this.projectPath);

        if (vuelidateBootFile.exists()) {
            const relativePath = relative(this.projectPath, vuelidateBootFile.filePath);
            console.log('process vuelidate boot file', relativePath);
            vuelidateBootFile.open();
            vuelidateBootFile.processVuelidateBootFile();
            const vuelidateBootFileString = vuelidateBootFile.save();

            if (this.dryRun) {
                console.log(vuelidateBootFileString);
            } else {
                const savePath = vuelidateBootFile.filePath;
                if (!savePath || !savePath.endsWith('.js')) {
                    console.error('unexpected file path, saving aborted:', savePath);
                    process.exit(1);
                }
                await writeFile(savePath, vuelidateBootFileString, 'utf8');
            }
        }
    }

    async getAllVueFilePaths () {
        const allFiles = await getFilesRecursive(join(this.projectPath, 'src'));
        return allFiles.filter(file => file.endsWith('.vue'));
    }

    executeInstall (packageName, dev = false) {
        const installPackageCommand = new InstallPackageCommand(this.projectPath, this.useYarn, this.useNpm);
        return installPackageCommand.execute(packageName, dev);
    }
}

module.exports = Vuelidate2ChangesCommand;
