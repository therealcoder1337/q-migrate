const fs = require('fs');
const {rmdir, rm} = fs.promises;
const {existsSync} = fs;
const {join} = require('path');
const InstallPackageCommand = require('../lib/InstallPackageCommand');

class InstallV2Command {
    constructor (projectPath, doClean, useYarn, useNpm) {
        this.projectPath = projectPath;
        this.nodeModulesPath = join(this.projectPath, 'node_modules');
        this.doClean = !!doClean;
        this.useYarn = !!useYarn;
        this.useNpm = !!useNpm;
    }

    async execute () {
        if (this.doClean) {
            await this.cleanForInstall();
        }

        console.log('installing packages in project', this.projectPath);
        await this.executeInstall('quasar@next');
        await this.executeInstall('@quasar/app@next', true);
    }

    async cleanForInstall () {
        console.log('clean: deleting folder', this.nodeModulesPath);
        const quasarFolderPath = join(this.projectPath, '.quasar');
        const packageLockPath = join(this.projectPath, 'package-lock.json');
        const yarnLockPath = join(this.projectPath, 'yarn.lock');

        await rmdir(this.nodeModulesPath, {recursive: true});
        if (existsSync(quasarFolderPath)) {
            console.log('clean: deleting folder', quasarFolderPath);
            await rmdir(quasarFolderPath, {recursive: true});
        }

        for (const filePath of [packageLockPath, yarnLockPath]) {
            if (existsSync(filePath)) {
                console.log('clean: deleting file', filePath);
                await rm(filePath);
            }
        }
    }

    executeInstall (packageName, dev = false) {
        const installPackageCommand = new InstallPackageCommand(this.projectPath, this.useYarn, this.useNpm);
        return installPackageCommand.execute(packageName, dev);
    }
}

module.exports = InstallV2Command;
