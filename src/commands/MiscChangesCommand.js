const CheckedJsFile = require('../lib/CheckedJsFile');
const QuasarVueFile = require('../lib/QuasarVueFile');
const getFilesRecursive = require('../helpers/get-files-recursive');
const {join, relative} = require('path');
const colors = require('colors/safe');
const fs = require('../get-fs')().promises;

class MiscChangesCommand {
    constructor (projectPath) {
        this.projectPath = projectPath;
    }

    async execute (doPrint = true) {
        const vueFilePaths = await this.getAllVueFilePaths();
        const jsFilePaths = await this.getAllJsFilePaths();
        const filesToResults = {
            'package.json': await this.checkPackageJson()
        };

        for (const vueFilePath of vueFilePaths) {
            const relativePath = relative(this.projectPath, vueFilePath);
            const quasarVueFile = new QuasarVueFile(this.projectPath, relativePath);

            quasarVueFile.open();

            console.log('process vue file', relativePath);

            const results = quasarVueFile.checkAll();

            filesToResults[relativePath] = results;
        }

        for (const jsFilePath of jsFilePaths) {
            const relativePath = relative(this.projectPath, jsFilePath);
            const jsFile = new CheckedJsFile(this.projectPath, relativePath);

            jsFile.open();

            console.log('process js file', relativePath);

            const results = jsFile.checkForOldVueApi();

            filesToResults[relativePath] = results;
        }

        if (doPrint) {
            console.log('');
            this.printResults(filesToResults);
        }

        return filesToResults;
    }

    printResults (filesToResults) {
        Object.entries(filesToResults)
            .filter(entry => !!entry[1].length)
            .forEach(entry => {
                const [file, results] = entry;
                console.log(colors.underline(`Results for file ${file}:`));
                console.log('');
                if (results.length) {
                    results.forEach(result => console.log(colors.red('    ' + result)));
                }
                console.log('');
            });
    }

    async getAllVueFilePaths () {
        const allFiles = await getFilesRecursive(join(this.projectPath, 'src'));
        return allFiles.filter(file => file.endsWith('.vue'));
    }

    async getAllJsFilePaths () {
        const allFiles = await getFilesRecursive(join(this.projectPath, 'src'));
        return allFiles.filter(file => file.endsWith('.js'));
    }

    async checkPackageJson () {
        const results = [];
        try {
            const buffer = await fs.readFile(join(this.projectPath, 'package.json'), 'utf8');
            const obj = JSON.parse(buffer);
            const {browserslist} = obj;
            const ieEntry = browserslist?.find(entry => entry.startsWith('ie '));

            if (ieEntry) {
                results.push(`browserslist property includes unsupported ie target "${ieEntry}" (internet explorer support was dropped for v2)`);
            }
        } catch (error) {
            console.error('error checking package.json:', error);
        }

        return results;
    }
}

module.exports = MiscChangesCommand;
