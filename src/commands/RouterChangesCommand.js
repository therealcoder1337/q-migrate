const {writeFile} = require('fs').promises;
const RouterRoutesFile = require('../lib/RouterRoutesFile');
const RouterIndexFile = require('../lib/RouterIndexFile');

class RouterChangesCommand {
    constructor (projectPath, dryRun) {
        this.projectPath = projectPath;
        this.routerRoutesFile = new RouterRoutesFile(this.projectPath);
        this.routerIndexFile = new RouterIndexFile(this.projectPath);
        this.dryRun = !!dryRun;
    }

    async execute () {
        this.routerRoutesFile.open();
        this.routerRoutesFile.change404Route();

        this.routerIndexFile.open();
        this.routerIndexFile.addImports();
        this.routerIndexFile.replaceInstantiation();
        this.routerIndexFile.removeVueUse();
        this.routerIndexFile.addCreateHistory();
        const routerIndex = this.routerIndexFile.save();
        const routes = this.routerRoutesFile.save();

        if (this.dryRun) {
            console.log(routerIndex);
            console.log(routes);
        } else {
            await writeFile(this.routerIndexFile.filePath, routerIndex, 'utf8');
            await writeFile(this.routerRoutesFile.filePath, routes, 'utf8');
        }
    }
}

module.exports = RouterChangesCommand;
