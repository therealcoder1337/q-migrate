const {writeFile} = require('fs').promises;
const StoreIndexFile = require('../lib/StoreIndexFile');

class VuexChangesCommand {
    constructor (projectPath, dryRun) {
        this.projectPath = projectPath;
        this.storeIndexFile = new StoreIndexFile(this.projectPath);
        this.dryRun = !!dryRun;
    }

    async execute () {
        this.storeIndexFile.open();
        this.storeIndexFile.addImports();
        this.storeIndexFile.replaceInstantiation();
        this.storeIndexFile.removeVueUse();
        this.storeIndexFile.transformStrict();

        const storeIndex = this.storeIndexFile.save();

        if (this.dryRun) {
            console.log(storeIndex);
        } else {
            await writeFile(this.storeIndexFile.filePath, storeIndex, 'utf8');
        }
    }
}

module.exports = VuexChangesCommand;
