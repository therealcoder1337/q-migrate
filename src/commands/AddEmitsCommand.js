const VueFile = require('../lib/VueFile');
const getFilesRecursive = require('../helpers/get-files-recursive');
const {join, relative} = require('path');
const {writeFile} = require('fs').promises;

class AddEmitsCommand {
    constructor (projectPath, dryRun) {
        this.projectPath = projectPath;
        this.dryRun = !!dryRun;
    }

    async execute () {
        const vueFilePaths = await this.getAllVueFilePaths();

        for (const vueFilePath of vueFilePaths) {
            const relativePath = relative(this.projectPath, vueFilePath);
            const vueFile = new VueFile(this.projectPath, relativePath);

            vueFile.open();

            if (!vueFile.usesEmit()) {
                continue;
            }

            console.log('process file', relativePath);

            vueFile.processEmits();

            const vueFileString = vueFile.save();

            if (this.dryRun) {
                console.log(vueFileString);
            } else {
                const savePath = vueFile.filePath;
                if (!savePath || !savePath.endsWith(relativePath)) {
                    console.error('unexpected file path, saving aborted:', savePath);
                    process.exit(1);
                }
                await writeFile(savePath, vueFileString, 'utf8');
            }
        }
    }

    async getAllVueFilePaths () {
        const allFiles = await getFilesRecursive(join(this.projectPath, 'src'));
        return allFiles.filter(file => file.endsWith('.vue'));
    }
}

module.exports = AddEmitsCommand;
