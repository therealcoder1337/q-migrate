module.exports = () => {
    if (process.env.NODE_ENV?.toLowerCase() === 'test') {
        return require('memfs').fs;
    }

    return require('fs');
};
