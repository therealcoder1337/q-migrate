const {join} = require('path');
const {visit} = require('recast');
const BaseJsFile = require('./BaseJsFile');

class RouterRoutesFile extends BaseJsFile {
    constructor (projectPath) {
        super(projectPath);
        this.filePath = join(projectPath, 'src', 'router', 'routes.js');
    }

    change404Route () {
        visit(this.parsed.program, {
            visitObjectExpression (node) {
                const props = node.value.properties;
                const hasComponent = props.some(prop => prop?.key?.name === 'component');
                const hasPath = props.some(prop => prop?.key?.name === 'path');

                if (!hasComponent || !hasPath) {
                    return this.traverse(node);
                }

                const pathProp = props.find(prop => prop?.key?.name === 'path');
                const isWildcard = pathProp && pathProp?.value?.value === '*';

                if (isWildcard) {
                    pathProp.value.value = '/:catchAll(.*)*';
                }

                this.traverse(node);
            }
        });
    }
}

module.exports = RouterRoutesFile;
