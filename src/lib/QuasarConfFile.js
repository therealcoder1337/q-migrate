const {join} = require('path');
const {visit} = require('recast');
const BaseJsFile = require('./BaseJsFile');
const fixLocale = require('./fix-locale');

class QuasarConfFile extends BaseJsFile {
    constructor (projectPath) {
        super(projectPath);
        this.filePath = join(projectPath, 'quasar.conf.js');
    }

    replaceOldLocales () {
        visit(this.parsed.program, {
            visitLiteral (node) {
                const parent = node.parent;
                const parentParent = parent?.parent;
                const parentFrameworkValue = parentParent?.parent?.value;

                if (parent?.value?.type === 'ObjectProperty' &&
                        parentParent?.value?.type === 'ObjectExpression' &&
                        parentFrameworkValue?.key?.name === 'framework') {
                    const propName = parent.value?.key.name;

                    if (propName === 'lang' && node.value.type === 'StringLiteral') {
                        const locale = node.value.value;
                        if (locale === locale.toLowerCase() && locale.includes('-')) {
                            node.value.value = fixLocale(locale);
                        }
                    }
                }

                this.traverse(node);
            }
        });
    }
}

module.exports = QuasarConfFile;
