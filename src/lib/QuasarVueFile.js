const {visit} = require('recast');
const VueFile = require('./VueFile');

class QuasarVueFile extends VueFile {
    checkAll () {
        this.checkForOldVueApi();

        const self = this;

        visit(this.parsed.program.templateBody, {
            visitVElement (path) {
                const element = path.value.name;
                if (element === 'q-uploader-base') {
                    self.addElementError({element});
                }

                this.traverse(path);
            },
            visitVIdentifier (path) {
                const attribute = path.value.name;
                const element = self.findElement(path.parent)?.value?.name || null;

                self.checkNativeModifier({path, attribute, element});
                self.checkSyncAttributes({path, attribute, element});
                self.checkTableDataAttribute({attribute, element});
                self.checkContentClassAndStyleAttributes({attribute, element});
                self.checkAppendAttribute({attribute, element});
                self.checkSanitizeAttributes({attribute, element});
                self.checkHorizontalAttribute({attribute, element});
                self.checkQImgAttributes({attribute, element});

                this.traverse(path);
            }
        });

        return this.foundIssues;
    }

    checkNativeModifier ({path, attribute, element}) {
        if (attribute === 'native' && path.parent?.value.modifiers?.some(mod => mod === path.value)) {
            const message = `element "${element}" uses removed ".native" modifier (consider adding related event to "emits" prop after removing modifier)`;
            this.foundIssues.push(message);
        }
    }

    checkContentClassAndStyleAttributes ({attribute, element}) {
        const elements = ['q-drawer', 'q-dialog', 'q-menu', 'q-tooltip'];
        const attributes = ['content-class', 'content-style'];
        if (attributes.includes(attribute) && elements.includes(element)) {
            const newAttribute = attribute.includes('-style') ? 'style' : 'class';
            this.addAttributeError({attribute, element, newAttribute});
        }
    }

    checkSyncAttributes ({path, attribute, element}) {
        const syncProps = [
            'pagination',
            'selected',
            'expanded'
        ];
        const elements = ['q-table', 'q-tree'];

        if (elements.includes(element) && syncProps.includes(attribute) && path.parent?.value.modifiers[0]?.name === 'sync') {
            this.addAttributeError({element, attribute: `${attribute}.sync`, newAttribute: `v-model:${attribute}`});
        }
    }

    checkAppendAttribute ({attribute, element}) {
        const elements = ['q-breadcrumbs-el', 'q-expansion-item'];
        if (attribute === 'append' && elements.includes(element)) {
            this.addAttributeError({attribute, element, newAttribute: false});
        }
    }

    checkHorizontalAttribute ({attribute, element}) {
        if (element === 'q-scroll-area' && attribute === 'horizontal') {
            this.addAttributeError({attribute, element, newAttribute: false});
        }
    }

    checkQImgAttributes ({attribute, element}) {
        const attributesToNew = {
            'basic': 'no-transition',
            'no-default-spinner': 'no-spinner'
        };
        const attributes = Object.keys(attributesToNew);

        if (element === 'q-img' && attributes.includes(attribute)) {
            const newAttribute = attributesToNew[attribute];
            this.addAttributeError({attribute, element, newAttribute});
        }
    }

    checkTableDataAttribute ({attribute, element}) {
        if (element === 'q-table' && attribute === 'data') {
            this.addAttributeError({attribute, element, newAttribute: 'rows'});
        }
    }

    checkSanitizeAttributes ({attribute, element}) {
        const attributes = [
            'label-sanitize',
            'name-sanitize',
            'text-sanitize',
            'stamp-sanitize'
        ];

        if (element === 'q-chat-message' && attributes.includes(attribute)) {
            this.addAttributeError({attribute, element, newAttribute: '!' + attribute.replace('-sanitize', '-html')});
        }
    }

    addAttributeError ({element, attribute, newAttribute}) {
        const newPart = newAttribute ? ` (new: "${newAttribute}")` : '';
        this.foundIssues.push(`element "${element || '<unknown element>'}" uses old attribute "${attribute}"` + newPart);
    }

    addElementError ({element}) {
        this.foundIssues.push(`element "${element}" was removed from v2`);
    }

    findElement (path) {
        let element = path.parent || {};
        while (element.value.type && element.value.type !== 'VElement') {
            element = element.parent || {};
        }

        return element;
    }
}

module.exports = QuasarVueFile;
