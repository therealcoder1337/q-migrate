const {join} = require('path');
const {visit} = require('recast');
const BaseJsFile = require('./BaseJsFile');
const fixLocale = require('./fix-locale');

class I18nIndexFile extends BaseJsFile {
    constructor (projectPath) {
        super(projectPath);
        this.filePath = join(projectPath, 'src', 'i18n', 'index.js');
    }

    replaceOldLocales () {
        visit(this.parsed.program, {
            visitLiteral (node) {
                const parent = node.parent;
                const parentParent = parent?.parent;
                if (parent?.value?.type === 'ObjectProperty' && parentParent?.value?.type === 'ObjectExpression' &&
                parentParent?.parent?.value?.type === 'ExportDefaultDeclaration') {
                    const propName = parent?.value?.key?.value;
                    if (typeof propName === 'string' && propName === propName.toLowerCase() &&
                            propName.includes('-')) {
                        node.value.value = fixLocale(propName);
                    }
                }

                this.traverse(node);
            }
        });
    }
}

module.exports = I18nIndexFile;
