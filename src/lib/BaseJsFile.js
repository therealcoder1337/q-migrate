const fs = require('../get-fs')();
const {parse, print, visit} = require('recast');
const removeDefaultImport = require('./recast/remove-default-import');
const ensureImport = require('./recast/ensure-import');
const ensureDefaultImport = require('./recast/ensure-default-import');

class BaseJsFile {
    constructor (projectPath) {
        this.projectPath = projectPath;
    }

    open () {
        if (!this.filePath) {
            throw new Error('no file path');
        }

        this.fileString = fs.readFileSync(this.filePath, 'utf8');
        this.parsed = parse(this.fileString, {
            parser: require('recast/parsers/babel')
        });
    }

    exists () {
        if (!this.filePath) {
            throw new Error('no file path');
        }

        return fs.existsSync(this.filePath);
    }

    save () {
        let code = print(this.parsed.program, {quote: 'single'}).code;
        if (!code.endsWith('\n')) {
            code += '\n';
        }

        return code;
    }

    removeDefaultImport (importPath) {
        return removeDefaultImport(this.parsed.program, importPath);
    }

    ensureImport (importElement, importPath) {
        return ensureImport(this.parsed.program, importElement, importPath);
    }

    ensureDefaultImport (importName, importPath) {
        return ensureDefaultImport(this.parsed.program, importName, importPath);
    }

    removeVueUse () {
        let nameFound = null;
        visit(this.parsed.program, {
            visitImportDeclaration (node) {
                if (node.value.source.value === 'vue' && node.value.specifiers.length === 1 &&
                        node.value.specifiers[0].type === 'ImportDefaultSpecifier') {
                    nameFound = node.value.specifiers[0].local.name;
                    node.prune();
                    return false;
                }
                this.traverse(node);
            },
            visitCallExpression (node) {
                if (!nameFound) {
                    return this.traverse(node);
                }

                if (node.value.callee?.object?.name === nameFound && node.value.callee?.property.name === 'use') {
                    node.prune();
                    return false;
                }

                return this.traverse(node);
            }
        });
    }
}

module.exports = BaseJsFile;
