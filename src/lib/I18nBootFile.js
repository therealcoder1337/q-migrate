const {join} = require('path');
const {visit, types} = require('recast');
const BaseJsFile = require('./BaseJsFile');
const fixLocale = require('./fix-locale');

class I18nBootFile extends BaseJsFile {
    constructor (projectPath) {
        super(projectPath);
        this.filePath = join(projectPath, 'src', 'boot', 'i18n.js');
    }

    addImports () {
        this.removeDefaultImport('vue-i18n');

        this.ensureImport('createI18n', 'vue-i18n');
    }

    replaceInstantiation () {
        visit(this.parsed.program, {
            visitNewExpression (node) {
                if (node.value.callee.name === 'VueI18n') {
                    node.value.type = 'CallExpression';
                    node.value.callee.name = 'createI18n';
                }
                this.traverse(node);
            }
        });
    }

    replaceOldLocales () {
        visit(this.parsed.program, {
            visitLiteral (node) {
                const parent = node.parent;
                const parentParent = parent?.parent;

                if (parent?.value?.type === 'ObjectProperty' && parentParent?.value?.type === 'ObjectExpression' &&
                        ['NewExpression', 'CallExpression'].includes(parentParent?.parent?.value?.type)) {
                    const propName = parent?.value?.key?.name;

                    if (['locale', 'fallbackLocale'].includes(propName) && node.value.type === 'StringLiteral') {
                        const locale = node.value.value;
                        if (locale === locale.toLowerCase() && locale.includes('-')) {
                            node.value.value = fixLocale(locale);
                        }
                    }
                }

                this.traverse(node);
            }
        });
    }

    useAppUse () {
        const self = this;
        visit(this.parsed.program, {
            visitIdentifier (path) {
                const parent = path.parent;
                const parentParent = parent?.parent;
                const parentParentParent = parentParent?.parent;

                if (path.value.name === 'i18n' && parent.value.type === 'MemberExpression' &&
                    parent.value.object.name === 'app' &&
                    parentParent?.value.type === 'AssignmentExpression' &&
                    parentParentParent.value.type === 'ExpressionStatement') {
                    const appUse = self.buildAppUseI18n();
                    parentParentParent.value.expression = appUse;
                }
                this.traverse(path);
            }
        });
    }

    buildAppUseI18n () {
        const b = types.builders;

        return b.callExpression(
            b.memberExpression(
                b.identifier('app'), b.identifier('use')
            ),
            [b.identifier('i18n')]
        );
    }
}

module.exports = I18nBootFile;
