const {join} = require('path');
const {visit} = require('recast');
const BaseJsFile = require('./BaseJsFile');

class StoreIndexFile extends BaseJsFile {
    constructor (projectPath) {
        super(projectPath);
        this.filePath = join(projectPath, 'src', 'store', 'index.js');
    }

    replaceInstantiation () {
        visit(this.parsed.program, {
            visitNewExpression (node) {
                if (node.value.callee?.object?.name === 'Vuex' && node.value.callee?.property?.name) {
                    node.value.type = 'CallExpression';
                    node.value.callee.name = 'createStore';
                    node.value.callee.type = 'Identifier';
                }
                this.traverse(node);
            }
        });
    }

    transformStrict () {
        visit(this.parsed.program, {
            visitObjectProperty (path) {
                if (path.parent.parent?.node?.type === 'CallExpression' &&
                path.parent.parent?.node?.callee?.name === 'createStore' &&
                path.value.value.type === 'MemberExpression' &&
                path.value.key.name === 'strict') {
                    const value = path.value?.value;
                    if (value.object?.type !== 'MemberExpression' || value.object.object?.name !== 'process' ||
                    value.object.property?.name !== 'env' || value.property?.name !== 'DEV') {
                        return this.traverse(path);
                    }

                    value.property.name = 'DEBUGGING';
                }

                this.traverse(path);
            }
        });
    }

    addImports () {
        this.removeDefaultImport('vuex');

        this.ensureImport('createStore', 'vuex');
    }
}

module.exports = StoreIndexFile;
