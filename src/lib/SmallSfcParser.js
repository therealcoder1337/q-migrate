/**
 * original tokenizer by https://gist.github.com/borgar/451393/7698c95178898c9466214867b46acb2ab2f56d68
 * we just use this because recast throws for vue sfcs which contain e. g. style blocks.
 * my guess is that the vue-eslint-parser doesn't add tokens to them or there are some type definitions i missed.
 * this one here is not to add tokens, but to allow extracting only <template> and <script> parts and later reassembling them.
 */

/*
 * Tiny tokenizer
 *
 * - Accepts a subject string and an object of regular expressions for parsing
 * - Returns an array of token objects
 *
 * tokenize('this is text.', { word:/\w+/, whitespace:/\s+/, punctuation:/[^\w\s]/ }, 'invalid');
 * result => [{ token="this", type="word" },{ token=" ", type="whitespace" }, Object { token="is", type="word" }, ... ]
 *
 */
function tokenize (s, parsers, deftok) {
    let m; let r; let t; const tokens = [];
    while (s) {
        t = null;
        m = s.length;
        /* eslint-disable guard-for-in */
        for (const key in parsers) {
            r = parsers[key].exec(s);
            // try to choose the best match if there are several
            // where "best" is the closest to the current starting point
            if (r && (r.index < m)) {
                t = {
                    token: r[0],
                    type: key,
                    matches: r.slice(1)
                };
                m = r.index;
            }
        }
        /* eslint-enable guard-for-in */

        if (m) {
        // there is text between last token and currently
        // matched token - push that out as default or "unknown"
            tokens.push({
                token: s.substr(0, m),
                type: deftok || 'unknown'
            });
        }
        if (t) {
        // push current token onto sequence
            tokens.push(t);
        }
        s = s.substr(m + (t ? t.token.length : 0));
    }
    return tokens;
}


class SmallSfcParser {
    parse (rawStr) {
        const tokens = this.getTokens(rawStr);
        const parseResult = this.doParse(tokens);
        const numOfTemplates = parseResult.reduce((carry, current) => {
            return current.type === 'template' ? carry + 1 : carry;
        }, 0);
        const numOfScripts = parseResult.reduce((carry, current) => {
            return current.type === 'script' ? carry + 1 : carry;
        }, 0);

        if (numOfTemplates !== 1) {
            throw new Error('unexpected number of template elements for sfc: ' + numOfTemplates);
        }

        if (numOfScripts !== 1) {
            throw new Error('unexpected number of script elements for sfc: ' + numOfScripts);
        }

        return {parseResult, simple: this.getSimpleRepresentation(parseResult)};
    }

    getSimpleRepresentation (parseResult) {
        const template = parseResult.find(t => t.type === 'template').value;
        const script = parseResult.find(t => t.type === 'script').value;

        return `<template>${template}</template>\n<script>${script}</script>\n`;
    }

    getTokens (str) {
        return tokenize(str, {
            templateStartTag: /^<template>/,
            templateEndTag: /(?<=[\n])<\/template>/,
            scriptStartTag: /<script(\stype="text\/javascript")?>/,
            scriptEndTag: /(?<=[\n])<\/script>/
        }, 'other');
    }

    doParse (tokens) {
        const iter = tokens[Symbol.iterator]();
        const parserError = new Error('parser error (e. g. unexpected tag order)');

        let result;
        let currentOpenTag = '';
        let currentOpenTagToken = '';
        let currentOthers = '';
        const parsedTags = [];

        while (!(result = iter.next()).done) {
            const tokenObj = result.value;
            const {type, token} = tokenObj;
            const isInvalidCase1 = currentOpenTag && (type.endsWith('StartTag') || (type.endsWith('EndTag') && type !== `${currentOpenTag}EndTag`));
            const isInvalidCase2 = !currentOpenTag && type.endsWith('EndTag');

            if (isInvalidCase1 || isInvalidCase2) {
                console.log(tokenObj, 'open:', currentOpenTag || 'none');
                throw parserError;
            }

            if (type.endsWith('StartTag')) {
                currentOpenTag = type.replace('StartTag', '');
                currentOpenTagToken = token;
                continue;
            }

            if (type.endsWith('EndTag')) {
                parsedTags.push({type: currentOpenTag, value: currentOthers, startTagToken: currentOpenTagToken});
                currentOpenTag = '';
                currentOpenTagToken = '';
                currentOthers = '';
                continue;
            }

            if (type === 'other') {
                if (!currentOpenTag) {
                    parsedTags.push({type: 'other', value: token});
                } else {
                    currentOthers += token;
                }
                continue;
            }

            throw new Error('unexpected parser state');
        }

        return parsedTags;
    }
}

module.exports = SmallSfcParser;
