const BaseJsFile = require('./BaseJsFile');
const {join} = require('path');
const {visit} = require('recast');

class CheckedJsFile extends BaseJsFile {
    constructor (projectPath, relativeFilePath) {
        super(projectPath, relativeFilePath);
        this.filePath = join(projectPath, relativeFilePath);
        this.foundIssues = [];
    }

    checkForOldVueApi () {
        const self = this;
        visit(this.parsed.program, {
            visitIdentifier (path) {
                const parent = path.parent;
                const parentParent = parent?.parent;
                if (path.value.name === 'Vue' &&
                    parent?.value.type === 'MemberExpression') {
                    const firstProp = parent.value.property.name;
                    const secondProp = parentParent?.value.type === 'MemberExpression' ? parentParent.value.property.name : null;
                    self.checkVueProps({firstProp, secondProp});
                }
                this.traverse(path);
            }
        });

        return this.foundIssues;
    }

    checkVueProps ({firstProp, secondProp}) {
        if (firstProp === 'config') {
            if (secondProp === 'productionTip') {
                return this.foundIssues.push('file uses old "Vue.config.productionTip"');
            }

            if (secondProp === 'ignoredElements') {
                return this.foundIssues.push('file uses old "Vue.config.ignoredElements" (new: "app.config.isCustomElement" [function])');
            }

            return this.foundIssues.push('file uses old "Vue.config" (new: "app.config")');
        }

        if (firstProp === 'prototype') {
            return this.foundIssues.push('file uses old "Vue.prototype" (new: "app.config.globalProperties")');
        }

        const nowApp = ['use', 'component', 'directive', 'mixin'];

        if (nowApp.includes(firstProp)) {
            this.foundIssues.push(`file uses old "Vue.${firstProp}" (new: "app.${firstProp}")`);
        }
    }
}

module.exports = CheckedJsFile;
