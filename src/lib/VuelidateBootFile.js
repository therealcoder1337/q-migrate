const BaseJsFile = require('./BaseJsFile');
const {visit} = require('recast');
const {join} = require('path');

class VuelidateBootFile extends BaseJsFile {
    constructor (projectPath) {
        super(projectPath);
        this.filePath = join(this.projectPath, 'src', 'boot', 'vuelidate.js');
    }

    processVuelidateBootFile () {
        this.ensureImport('VuelidatePlugin', '@vuelidate/core');
        this.removeDefaultImport('vuelidate');

        visit(this.parsed.program, {
            visitObjectPattern (path) {
                const parentParent = path.parent?.parent;
                const vueProperty = path.value.properties
                    .find(entry => entry.key?.name === entry.value?.name && entry.key.name === 'Vue');

                if (parentParent?.value.type === 'ExportDefaultDeclaration' && vueProperty) {
                    vueProperty.key.name = 'app';
                    vueProperty.value.name = 'app';
                }

                this.traverse(path);
            },
            visitCallExpression (path) {
                if (path.value.callee?.type === 'MemberExpression' &&
                        path.value.callee?.object?.name === 'Vue' &&
                        path.value.callee?.property?.name === 'use' &&
                        path.value.arguments[0]?.name === 'Vuelidate') {
                    path.value.callee.object.name = 'app';
                    path.value.arguments[0].name = 'VuelidatePlugin';
                }

                this.traverse(path);
            }
        });
    }
}

module.exports = VuelidateBootFile;
