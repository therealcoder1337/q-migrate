const {parse, visit, types, print} = require('recast');
const {uniq} = require('underscore');
const CheckedJsFile = require('./CheckedJsFile');
const vueEslintParser = require('vue-eslint-parser');
const SmallSfcParser = require('./SmallSfcParser');
const fs = require('../get-fs')();
const initVueTypes = require('./recast/init-vue-types');
initVueTypes();

class VueFile extends CheckedJsFile {
    constructor (projectPath, relativeFilePath) {
        super(projectPath, relativeFilePath);
        this.parseResult = null;
    }

    open () {
        if (!this.filePath) {
            throw new Error('no file path');
        }

        this.fileString = fs.readFileSync(this.filePath, 'utf8');
        const smallSfcParser = new SmallSfcParser();
        const {simple, parseResult} = smallSfcParser.parse(this.fileString);
        this.parsed = parse(simple, {
            parser: this.getParser()
        });
        this.parseResult = parseResult;
    }

    getTemplateCode () {
        return print(this.parsed.program.templateBody, {quote: 'single'}).code;
    }

    save () {
        const code = super.save();

        return this.parseResult.map(node => {
            if (node.type === 'template') {
                return this.getTemplateCode();
            }

            if (node.type === 'script') {
                return `${node.startTagToken}\n${code}\n</script>`;
            }

            return node.value;
        }).join('');
    }

    getParser () {
        return {
            parse (source) {
                const parsed = vueEslintParser.parse(source, {
                    parser: require('recast/parsers/babel'),
                    ecmaVersion: 2020,
                    range: true,
                    loc: true,
                    comment: true,
                    tokens: true,
                    sourceType: 'module'
                });

                return parsed;
            }
        };
    }

    usesEmit () {
        return this.getEmittedEvents().length;
    }

    getEmittedEvents () {
        const emitted = [];
        visit(this.parsed.program, {
            visitIdentifier (node) {
                const parent = node.parent;
                const parentParent = parent?.parent;

                if (node.value.name === '$emit' &&
                        parent?.value?.object?.type === 'ThisExpression' &&
                        parentParent?.value.type === 'CallExpression' &&
                        parentParent?.value.arguments[0]?.type === 'Literal') {
                    const evtName = parentParent.value.arguments[0].value;
                    emitted.push(evtName);
                }

                this.traverse(node);
            }
        });

        return uniq(emitted);
    }

    processEmits () {
        const emitted = this.getEmittedEvents();

        if (!emitted.length) {
            return;
        }

        const arr = this.ensureEmitsProp();
        const existing = arr.elements.filter(el => el.type === 'Literal').map(el => el.value);
        const add = emitted.filter(el => !existing.includes(el));

        add.forEach(el => arr.elements.push(this.buildLiteral(el)));
    }

    addToEmits () {
        const self = this;

        visit(this.parsed.program, {
            visitObjectExpression (node) {
                const parent = node.parent;
                if (parent.value.type === 'ExportDefaultDeclaration') {
                    node.value.properties.unshift(self.buildVuelidateSetupProp());
                }
                this.traverse(node);
            }
        });
    }

    ensureEmitsProp () {
        const self = this;
        let emitsArray = false;

        visit(this.parsed.program, {
            visitProperty (node) {
                const parentParent = node.parent?.parent;

                if (parentParent?.value.type === 'ExportDefaultDeclaration' &&
                        node.value.key?.name === 'emits' &&
                        node.value.value?.type === 'ArrayExpression') {
                    emitsArray = node.value.value;
                }

                this.traverse(node);
            }
        });

        if (!emitsArray) {
            visit(this.parsed.program, {
                visitObjectExpression (node) {
                    const parent = node.parent;

                    if (parent?.value.type === 'ExportDefaultDeclaration') {
                        const emitsProp = self.buildEmitsProp();
                        node.value.properties.push(emitsProp);
                        emitsArray = emitsProp.value;
                    }

                    this.traverse(node);
                }
            });
        }

        return emitsArray;
    }

    buildEmitsProp () {
        const b = types.builders;
        const emitProp = b.property('init',
            b.identifier('emits'),
            b.arrayExpression([])
        );
        return emitProp;
    }

    buildLiteral (value) {
        const b = types.builders;
        return b.literal(value);
    }
}

module.exports = VueFile;
