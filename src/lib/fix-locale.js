module.exports = function fixLocale (locale) {
    const parts = locale.split('-');
    const partsNew = parts.map((part, idx) => {
        const isLast = idx === (parts.length - 1);
        if (!isLast) {
            return part;
        }

        return part === 'latn' ? 'Latn' : part.toUpperCase();
    });

    return partsNew.join('-');
};
