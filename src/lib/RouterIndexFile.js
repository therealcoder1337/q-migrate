const {visit, types} = require('recast');
const BaseJsFile = require('./BaseJsFile');
const {join} = require('path');

class RouterIndexFile extends BaseJsFile {
    constructor (projectPath) {
        super(projectPath);
        this.filePath = join(this.projectPath, 'src', 'router', 'index.js');
    }

    replaceInstantiation () {
        visit(this.parsed.program, {
            visitNewExpression (node) {
                if (node.value.callee.name === 'VueRouter') {
                    node.value.type = 'CallExpression';
                    node.value.callee.name = 'createRouter';
                }
                this.traverse(node);
            }
        });
    }

    addImports () {
        this.removeDefaultImport('vue-router');

        const imports = ['createRouter', 'createMemoryHistory', 'createWebHistory',
            'createWebHashHistory'];

        imports.forEach(importName => this.ensureImport(importName, 'vue-router'));
    }

    addCreateHistory () {
        const self = this;
        visit(this.parsed.program, {
            visitExportDefaultDeclaration (path) {
                const body = path.value.declaration?.body?.body;
                if (Array.isArray(body) && !self.hasCreateHistoryDeclaration()) {
                    const createHistory = self.buildCreateHistory();
                    body.unshift(createHistory);
                }

                return this.traverse(path);
            },
            visitObjectExpression (path) {
                if (path.parent.node?.type === 'CallExpression' && path.parent.node?.callee?.name === 'createRouter') {
                    const alreadyHasHistoryProp = path.value.properties.some(prop => {
                        return prop.key.name === 'history';
                    });

                    if (!alreadyHasHistoryProp) {
                        path.value.properties.push(self.buildHistoryProp());
                    }
                }
                return this.traverse(path);
            },
            visitObjectProperty (path) {
                if (path.parent.parent?.node?.type === 'CallExpression' &&
                        path.parent.parent?.node?.callee?.name === 'createRouter' &&
                        path.value.value.type === 'MemberExpression') {
                    const key = path.value.key.name;
                    const value = path.value?.value;
                    if (!['mode', 'base'].includes(key) || value.type !== 'MemberExpression') {
                        return this.traverse(path);
                    }

                    if (value.object?.object?.name === 'process' && value.object?.property?.name === 'env' &&
                    ['VUE_ROUTER_MODE', 'VUE_ROUTER_BASE'].includes(value.property?.name)) {
                        path.prune();
                    }
                }

                return this.traverse(path);
            }
        });
    }

    hasCreateHistoryDeclaration () {
        let has = false;
        visit(this.parsed.program, {
            visitVariableDeclarator (path) {
                if (path.value.id?.name === 'createHistory') {
                    has = true;
                }

                return this.traverse(path);
            }
        });

        return has;
    }

    buildCreateHistory () {
        const b = types.builders;
        const createHistory = b.variableDeclaration('const', [
            b.variableDeclarator(
                b.identifier('createHistory'),
                b.conditionalExpression(
                    b.binaryExpression('===', b.memberExpression(
                        b.memberExpression(
                            b.identifier('process'),
                            b.identifier('env')
                        ),
                        b.identifier('MODE')
                    ), b.literal('ssr')),
                    b.identifier('createMemoryHistory'),
                    b.conditionalExpression(
                        b.binaryExpression('===', b.memberExpression(
                            b.memberExpression(
                                b.identifier('process'),
                                b.identifier('env')
                            ),
                            b.identifier('VUE_ROUTER_MODE')
                        ), b.literal('history')),
                        b.identifier('createWebHistory'),
                        b.identifier('createWebHashHistory')
                    )
                )
            )
        ]);
        return createHistory;
    }

    buildHistoryProp () {
        const b = types.builders;

        return b.property(
            'init',
            b.identifier('history'),
            b.callExpression(
                b.identifier('createHistory'), [
                    b.conditionalExpression(
                        b.binaryExpression(
                            '===',
                            b.memberExpression(
                                b.memberExpression(b.identifier('process'), b.identifier('env')),
                                b.identifier('MODE')
                            ),
                            b.literal('ssr')
                        ),
                        b.unaryExpression('void', b.literal(0)),
                        b.memberExpression(
                            b.memberExpression(b.identifier('process'), b.identifier('env')),
                            b.identifier('VUE_ROUTER_BASE')
                        )
                    )
                ]
            )
        );
    }
}

module.exports = RouterIndexFile;
