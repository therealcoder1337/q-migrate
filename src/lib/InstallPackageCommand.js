const {existsSync} = require('fs');
const {join} = require('path');
const {spawn} = require('child_process');
const isWin = process.platform === 'win32';

// this is not a cli-command, but a reusable class following the command pattern
class InstallPackageCommand {
    constructor (projectPath, useYarn, useNpm) {
        this.projectPath = projectPath;
        this.useYarn = !!useYarn;
        this.useNpm = !!useNpm;
        this.useAutodetect = !this.useYarn && !this.useNpm;
    }

    execute (packageName, dev = false) {
        const {spawnCmd, args} = this.getSpawnCmdAndArgs(packageName, dev);
        console.log(`running ${spawnCmd} ${args.join(' ')}`);

        return new Promise((resolve, reject) => {
            const npm = spawn(spawnCmd, args, {cwd: this.projectPath, stdio: 'inherit'});

            npm.on('close', (code) => {
                if (code > 0) {
                    console.error('npm exited with non-zero code', code);
                    return reject(new Error('code ' + code));
                }

                resolve();
            });
        });
    }

    willUseYarn () {
        let willUseYarn = false;

        if (this.useAutodetect) {
            const yarnLockPath = join(this.projectPath, 'yarn.lock');
            willUseYarn = existsSync(yarnLockPath);
            console.log(`autodetect: using ${willUseYarn ? 'yarn' : 'npm'} (use --yarn or --npm to override)`);
        } else if (this.useYarn) {
            willUseYarn = true;
            console.log('using yarn (as specified)');
        } else {
            console.log('using npm (as specified)');
        }

        return willUseYarn;
    }

    getSpawnCmdAndArgs (packageName, dev) {
        const willUseYarn = this.willUseYarn();

        const spawnCmd = `${willUseYarn ? 'yarn' : 'npm'}${isWin ? '.cmd' : ''}`;
        const args = willUseYarn ? this.getYarnArgs(packageName, dev) : this.getNpmArgs(packageName, dev);
        return {spawnCmd, args};
    }

    getYarnArgs (packageName, dev) {
        const args = ['add', packageName];

        if (dev) {
            args.push('--dev');
        }

        return args;
    }

    getNpmArgs (packageName, dev) {
        const args = ['install', packageName];

        args.push(dev ? '--save-dev' : '--save');

        return args;
    }
}

module.exports = InstallPackageCommand;
