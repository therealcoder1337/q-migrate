const {types, visit} = require('recast');
const VueFile = require('./VueFile');

class VuelidateFile extends VueFile {
    constructor (projectPath, relativeFilePath) {
        super(projectPath, relativeFilePath);
        this.replaceDollarInTemplate = false;
    }

    getTemplateCode () {
        const templateCode = super.getTemplateCode();
        return this.replaceDollarInTemplate ?
            templateCode.replace(/(?<=")\$v(?=[".])/g, 'v$') :
            templateCode;
    }

    isVuelidateFile () {
        let isVuelidateFile = false;
        visit(this.parsed.program, {
            visitLiteral (node) {
                const parent = node.parent;
                if (parent.value.type === 'ImportDeclaration' && node.value.value.includes('vuelidate')) {
                    isVuelidateFile = true;
                }
                this.traverse(node);
            }
        });

        return isVuelidateFile;
    }

    replaceDollar () {
        this.replaceDollarInTemplate = true;
        visit(this.parsed.program, {
            visitIdentifier (path) {
                if (path.value.name === '$v') {
                    path.value.name = 'v$';
                }

                this.traverse(path);
            }
        });
    }

    rewriteValidatorImports () {
        visit(this.parsed.program, {
            visitLiteral (node) {
                const parent = node.parent;
                if (parent.value.type === 'ImportDeclaration') {
                    if (node.value.value.startsWith('vuelidate/lib/validators')) {
                        // vuelidate 1 => vuelidate 2
                        node.value.value = node.value.value.replace('vuelidate/lib/validators', '@vuelidate/validators');
                    }

                    if (node.value.value.includes('validators/')) {
                        const b = types.builders;

                        // fix old direct import of a validator, but only if it was a default import (which should be the case)
                        if (parent.value.specifiers.some(specifier => specifier.type === 'ImportDefaultSpecifier')) {
                            const [specifier] = parent.value.specifiers;
                            const validatorName = node.value.value.split('/').slice(-1)[0];
                            specifier.type = 'ImportSpecifier';
                            specifier.imported = b.identifier(validatorName);
                            node.value.value = '@vuelidate/validators';
                        }
                    }
                }
                this.traverse(node);
            }
        });
    }

    addVuelidateSetup () {
        const self = this;

        if (this.hasSetupProp()) {
            return;
        }

        visit(this.parsed.program, {
            visitObjectExpression (node) {
                const parent = node.parent;
                if (parent.value.type === 'ExportDefaultDeclaration') {
                    node.value.properties.unshift(self.buildVuelidateSetupProp());
                }
                this.traverse(node);
            }
        });
    }

    hasSetupProp () {
        let hasSetupProp = false;

        visit(this.parsed.program, {
            visitProperty (node) {
                const parentParent = node.parent?.parent;

                if (parentParent?.value.type === 'ExportDefaultDeclaration' && node.value.key?.name === 'setup') {
                    hasSetupProp = true;
                }

                this.traverse(node);
            }
        });

        return hasSetupProp;
    }

    buildVuelidateSetupProp () {
        const b = types.builders;
        const setup = b.property('init', b.identifier('setup'), b.arrowFunctionExpression(
            [],
            b.objectExpression([
                b.property('init', b.identifier('v$'), b.callExpression(
                    b.identifier('useVuelidate'),
                    []
                ))
            ])
        ));
        return setup;
    }
}

module.exports = VuelidateFile;
