const {types} = require('recast');
const {builders} = types;

module.exports = (program, importElement, importPath) => {
    const {body} = program;
    const imports = body.filter(child => child && child.type === 'ImportDeclaration');

    const existingImports = imports.filter(el => el.source.value === importPath);
    const hasImport = existingImports.some(el =>
        el.specifiers.some(specifier => specifier.imported?.name === importElement));

    if (hasImport) {
        return;
    }

    let target = existingImports[0];

    if (!target) {
        target = builders.importDeclaration([builders.importSpecifier(builders.identifier(importElement))], builders.literal(importPath));
        body.unshift(target);
        return;
    }

    const newSpecifier = builders.importSpecifier(builders.identifier(importElement));

    target.specifiers.push(newSpecifier);
};
