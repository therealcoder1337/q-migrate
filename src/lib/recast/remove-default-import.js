module.exports = (program, importPath) => {
    const {body} = program;
    body.forEach((child) => {
        if (child.type !== 'ImportDeclaration' || child.source.value !== importPath) {
            return;
        }

        const isDefaultImport = child.specifiers
            .some(specifier => specifier.type === 'ImportDefaultSpecifier');

        if (isDefaultImport) {
            const idx = body.indexOf(child);
            if (idx > -1) {
                body.splice(idx, 1);
            }
        }
    });
};
