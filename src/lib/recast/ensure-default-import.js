const {types} = require('recast');
const {builders} = types;

module.exports = (program, importElement, importPath) => {
    const {body} = program;
    const imports = body.filter(child => child && child.type === 'ImportDeclaration');

    const existingImports = imports.filter(el => el.source.value === importPath && el.specifiers.every(specifier => specifier.type === 'ImportDefaultSpecifier'));
    const hasImport = existingImports.some(el =>
        el.specifiers.some(specifier => specifier.local?.name === importElement));

    if (hasImport) {
        return;
    }

    const target = builders.importDeclaration([builders.importDefaultSpecifier(builders.identifier(importElement))], builders.literal(importPath));
    body.unshift(target);
};
