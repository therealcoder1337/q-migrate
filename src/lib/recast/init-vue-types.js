const {types} = require('recast');
const {Type, builtInTypes, finalize} = types;

module.exports = () => {
    const def = Type.def;
    const or = Type.or;
    const {string, object} = builtInTypes;

    def('VIdentifier')
        .bases('Node')
        .build('name', 'rawName')
        .field('name', string)
        .field('rawName', string);

    def('VText')
        .bases('Node')
        .build('value')
        .field('value', string);

    def('VExpressionContainer')
        .bases('Node')
        .build('expression', 'references')
        .field('expression', or(def('Expression'), null))
        .field('references', def('Reference'));

    def('VForExpression')
        .bases('Expression')
        .build('left', 'right')
        .field('left', [def('Pattern')])
        .field('right', def('Expression'));

    def('VOnExpression')
        .bases('Expression')
        .build('body')
        .field('body', [def('Statement')]);

    def('VSlotScopeExpression')
        .bases('Expression')
        .build('params')
        .field('params', [or(def('Pattern'), def('RestElement'))]);

    def('VFilterSequenceExpression')
        .bases('Expression')
        .build('expression', 'filters')
        .field('expression', def('Expression'))
        .field('filters', [def('VFilter')]);

    def('VFilter')
        .bases('Node')
        .build('callee', 'arguments')
        .field('callee', def('Identifier'))
        .field('arguments', [def('Expression')]);

    def('VDirectiveKey')
        .bases('Node')
        .build('name', 'argument', 'modifiers')
        .field('name', def('VIdentifier'))
        .field('argument', or(def('VExpressionContainer'), def('VIdentifier'), null))
        .field('modifiers', [def('VIdentifier')]);

    def('VLiteral')
        .bases('Node')
        .build('type', 'value')
        .field('type', def('VAttributeValue'))
        .field('value', string);

    def('VAttribute')
        .bases('Node')
        .build('directive', 'key', 'value')
        .field('directive', false)
        .field('key', def('VIdentifier'))
        .field('value', or(def('VExpressionContainer', null)));

    def('VDirective')
        .bases('VAttribute')
        .build('directive', 'key', 'value')
        .field('directive', true)
        .field('key', def('VDirectiveKey'))
        .field('value', or(def('VExpressionContainer', null)));

    def('VStartTag')
        .bases('Node')
        .build('attributes')
        .field('attributes', [def('VAttribute')]);

    def('VEndTag')
        .bases('Node');

    def('VElement')
        .bases('Node')
        .build('namespace', 'name', 'startTag', 'children', 'endTag', 'variables')
        .field('namespace', string)
        .field('name', string)
        .field('startTag', def('VStartTag'))
        .field('children', [or(def('VText'), def('VExpressionContainer'), def('VElement'))])
        .field('endTag', or(def('VEndTag'), null))
        .field('variables', [object]);

    def('VRootElement')
        .bases('VElement')
        .build('tokens', 'comments', 'errors')
        .field('tokens', [object])
        .field('comments', [object])
        .field('errors', [object]);

    def('VDocumentFragment')
        .bases('Node')
        .build('children')
        .field('children', [or(def('VElement'), def('VText'), def('VExpressionContainer'))]);

    finalize();
};
