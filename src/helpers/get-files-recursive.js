// original: https://stackoverflow.com/a/45130990

const {resolve} = require('path');
const defaultFs = require('../get-fs')().promises;

async function getFilesRecursive (dir, fs = defaultFs) {
    const {readdir, stat} = fs;
    const subdirs = await readdir(dir);
    const files = await Promise.all(subdirs.map(async (subdir) => {
        const res = resolve(dir, subdir);
        return (await stat(res)).isDirectory() ? getFilesRecursive(res, fs) : res;
    }));
    return files.reduce((a, f) => a.concat(f), []);
}

module.exports = getFilesRecursive;
