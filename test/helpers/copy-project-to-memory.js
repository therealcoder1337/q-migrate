const getFilesRecursive = require('../../src/helpers/get-files-recursive');
const toMemPath = require('./to-mem-path');
const fs = require('fs').promises;
const memfs = require('memfs').fs.promises;
const {dirname} = require('path');

module.exports = async projectPath => {
    const filePaths = await getFilesRecursive(projectPath, fs);
    for (const filePath of filePaths) {
        const buffer = await fs.readFile(filePath, 'utf8');
        const memPath = toMemPath(filePath, projectPath);
        await memfs.mkdir(dirname(memPath), {recursive: true});
        await memfs.writeFile(memPath, buffer, 'utf8');
    }
};
