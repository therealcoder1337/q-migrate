const {join, relative} = require('path');

module.exports = function toMemPath (localPath, stripPath) {
    const relativePath = relative(stripPath, localPath);
    return join('/', 'memfs', relativePath);
};
