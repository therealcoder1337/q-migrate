const {join} = require('path');
const fixturesPath = join(__dirname, '..', '..', 'fixtures');
const MiscChangesCommand = require('../../../src/commands/MiscChangesCommand');
const beforePath = join(fixturesPath, 'project', 'before');
const copyProjectToMemory = require('../../helpers/copy-project-to-memory');

describe('MiscChangesCommand', () => {
    before(() => copyProjectToMemory(beforePath));

    function assertHasAttributeMessage (messages, element, attribute, attributeNew) {
        const expected = `element "${element}" uses old attribute "${attribute}"` + (attributeNew ? ` (new: "${attributeNew}")` : '');
        messages.includes(expected).should.eql(true);
    }

    function assertHasElementRemovedMessage (messages, element) {
        const expected = `element "${element}" was removed from v2`;
        messages.includes(expected).should.eql(true);
    }

    it('should correctly warn about old attributes, elements and other stuff', async () => {
        const command = new MiscChangesCommand('/memfs');
        const results = await command.execute(false);
        const relativeVuePath = 'src/components/deprecated-component.vue';
        const relativeJsPath = 'src/some-file-using-vue.js';
        Object.keys(results).length.should.eql(5);
        const allVueFileMessages = results[relativeVuePath];
        const allJsFileMessages = results[relativeJsPath];
        allVueFileMessages.length.should.eql(27);
        allJsFileMessages.should.eql([
            'file uses old "Vue.prototype" (new: "app.config.globalProperties")',
            'file uses old "Vue.use" (new: "app.use")',
            'file uses old "Vue.config" (new: "app.config")',
            'file uses old "Vue.config.productionTip"',
            'file uses old "Vue.config.ignoredElements" (new: "app.config.isCustomElement" [function])',
            'file uses old "Vue.component" (new: "app.component")',
            'file uses old "Vue.directive" (new: "app.directive")',
            'file uses old "Vue.mixin" (new: "app.mixin")'
        ]);

        results['package.json'].should.eql(['browserslist property includes unsupported ie target "ie >= 11" (internet explorer support was dropped for v2)']);

        results['src/boot/i18n.js'].should.eql(['file uses old "Vue.use" (new: "app.use")']);
        results['src/i18n/index.js'].should.eql([]);

        allVueFileMessages.includes('element "div" uses removed ".native" modifier (consider adding related event to "emits" prop after removing modifier)').should.eql(true);

        allVueFileMessages.includes('file uses old "Vue.prototype" (new: "app.config.globalProperties")').should.eql(true);

        assertHasAttributeMessage(allVueFileMessages, 'q-drawer', 'content-class', 'class');
        assertHasAttributeMessage(allVueFileMessages, 'q-drawer', 'content-style', 'style');

        assertHasAttributeMessage(allVueFileMessages, 'q-dialog', 'content-class', 'class');
        assertHasAttributeMessage(allVueFileMessages, 'q-dialog', 'content-style', 'style');

        assertHasAttributeMessage(allVueFileMessages, 'q-menu', 'content-class', 'class');
        assertHasAttributeMessage(allVueFileMessages, 'q-menu', 'content-style', 'style');

        assertHasAttributeMessage(allVueFileMessages, 'q-breadcrumbs-el', 'append');

        assertHasAttributeMessage(allVueFileMessages, 'q-tooltip', 'content-class', 'class');
        assertHasAttributeMessage(allVueFileMessages, 'q-tooltip', 'content-style', 'style');

        assertHasAttributeMessage(allVueFileMessages, 'q-chat-message', 'label-sanitize', '!label-html');
        assertHasAttributeMessage(allVueFileMessages, 'q-chat-message', 'name-sanitize', '!name-html');
        assertHasAttributeMessage(allVueFileMessages, 'q-chat-message', 'text-sanitize', '!text-html');
        assertHasAttributeMessage(allVueFileMessages, 'q-chat-message', 'stamp-sanitize', '!stamp-html');

        assertHasAttributeMessage(allVueFileMessages, 'q-expansion-item', 'append');

        assertHasAttributeMessage(allVueFileMessages, 'q-scroll-area', 'horizontal');

        assertHasAttributeMessage(allVueFileMessages, 'q-table', 'data', 'rows');

        assertHasAttributeMessage(allVueFileMessages, 'q-table', 'pagination.sync', 'v-model:pagination');
        assertHasAttributeMessage(allVueFileMessages, 'q-table', 'selected.sync', 'v-model:selected');
        assertHasAttributeMessage(allVueFileMessages, 'q-table', 'expanded.sync', 'v-model:expanded');

        assertHasAttributeMessage(allVueFileMessages, 'q-tree', 'pagination.sync', 'v-model:pagination');
        assertHasAttributeMessage(allVueFileMessages, 'q-tree', 'selected.sync', 'v-model:selected');
        assertHasAttributeMessage(allVueFileMessages, 'q-tree', 'expanded.sync', 'v-model:expanded');

        assertHasAttributeMessage(allVueFileMessages, 'q-img', 'basic', 'no-transition');
        assertHasAttributeMessage(allVueFileMessages, 'q-img', 'no-default-spinner', 'no-spinner');

        assertHasElementRemovedMessage(allVueFileMessages, 'q-uploader-base');
    });
});
