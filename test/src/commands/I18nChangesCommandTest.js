const {join} = require('path');
const fs = require('fs').promises;
const memfs = require('../../../src/get-fs')().promises;
const fixturesPath = join(__dirname, '..', '..', 'fixtures');
const I18nChangesCommand = require('../../../src/commands/I18nChangesCommand');
const getFilesRecursive = require('../../../src/helpers/get-files-recursive');
const beforePath = join(fixturesPath, 'project', 'before');
const afterPath = join(fixturesPath, 'project', 'after');
const copyProjectToMemory = require('../../helpers/copy-project-to-memory');
const toMemPath = require('../../helpers/to-mem-path');

describe('I18nChangesCommand', () => {
    before(() => copyProjectToMemory(beforePath));

    it('should correctly transform old code to new', async () => {
        const command = new I18nChangesCommand('/memfs');
        await command.execute();
        const afterChangesPath = join(afterPath, 'i18n-changes');
        const filePaths = await getFilesRecursive(afterChangesPath, fs);

        for (const localPath of filePaths) {
            const expected = await fs.readFile(localPath, 'utf8');
            const actual = await memfs.readFile(toMemPath(localPath, afterChangesPath), 'utf8');
            actual.trim().should.eql(expected.trim());
        }
    });
});
