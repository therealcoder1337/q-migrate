export default function ({Vue}) {
    Vue.prototype.$axios = window.axios; // now app.config.globalProperties
    Vue.use(SomePlugin); // now app.use
    Vue.config.test = 'some';
    Vue.config.productionTip = false; // got removed
    Vue.config.ignoredElements = []; // now app.config.isCustomElement
    Vue.component() // now app.component
    Vue.directive() // now app.directive
    Vue.mixin() // now app.mixin
};
