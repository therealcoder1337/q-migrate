import enUS from './en-us';
import deDe from './de-de';

export default {
    'en-us': enUS,
    'de-de': deDe
};
