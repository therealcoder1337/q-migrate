const program = require('commander');
const colors = require('colors');
const StylusToScssCommand = require('./src/commands/StylusToScssCommand');
const InstallV2Command = require('./src/commands/InstallV2Command');
const RouterChangesCommand = require('./src/commands/RouterChangesCommand');
const VuexChangesCommand = require('./src/commands/VuexChangesCommand');
const I18nChangesCommand = require('./src/commands/I18nChangesCommand');
const Vuelidate2ChangesCommand = require('./src/commands/Vuelidate2ChangesCommand');
const AddEmitsCommand = require('./src/commands/AddEmitsCommand');
const MiscChangesCommand = require('./src/commands/MiscChangesCommand');

function showUsageError () {
    program.outputHelp(colors.red);
    process.exit(1);
}

program.requiredOption('-p, --path <project path>', 'path to quasar project');

const dryRunOptionArgs = ['-d, --dry-run', 'print changes instead of saving them'];

function optsForInstall (params, opts) {
    opts.yarn = params.yarn || false;
    opts.npm = params.npm || false;

    if (opts.yarn && opts.npm) {
        console.error('error: only pass npm OR yarn option, not both');
        process.exit(1);
    }
}

program
    .command('stylus-to-scss')
    .action(() => {
        const opts = program.opts();
        (new StylusToScssCommand(opts.path)).execute();
    });

program
    .command('install-v2')
    .option('-c, --clean', 'remove node_modules, lock files and .quasar folder before installing')
    .option('-y, --yarn', 'use yarn instead of npm')
    .option('-n, --npm', 'use npm instead of yarn')
    .action(params => {
        const opts = program.opts();
        opts.clean = params.clean || false;
        optsForInstall(params, opts);

        (new InstallV2Command(opts.path, opts.clean, opts.yarn, opts.npm)).execute();
    });

program
    .command('router-changes')
    .option(...dryRunOptionArgs)
    .action(params => {
        const opts = program.opts();
        opts.dryRun = params.dryRun || false;
        (new RouterChangesCommand(opts.path, opts.dryRun)).execute();
    });

program
    .command('vuex-changes')
    .option(...dryRunOptionArgs)
    .action(params => {
        const opts = program.opts();
        opts.dryRun = params.dryRun || false;
        (new VuexChangesCommand(opts.path, opts.dryRun)).execute();
    });

program
    .command('i18n-changes')
    .option(...dryRunOptionArgs)
    .option('-i, --install', 'install vue-i18n@rc')
    .option('-y, --yarn', '(when installing) use yarn instead of npm')
    .option('-n, --npm', '(when installing) use npm instead of yarn')
    .action(params => {
        const opts = program.opts();
        opts.dryRun = params.dryRun || false;
        opts.install = params.install || false;
        optsForInstall(params, opts);

        if ((opts.npm || opts.yarn) && !opts.install) {
            console.error('error: passed yarn or npm option without install option');
            process.exit(1);
        }

        (new I18nChangesCommand(opts.path, opts.dryRun, opts.install, opts.yarn, opts.npm)).execute();
    });

program
    .command('vuelidate2-changes')
    .option(...dryRunOptionArgs)
    .option('-i, --install', 'install @vuelidate/core and @vuelidate/validators')
    .option('-y, --yarn', '(when installing) use yarn instead of npm')
    .option('-n, --npm', '(when installing) use npm instead of yarn')
    .option('-s, --setup', 'adds useVuelidate and a setup method (if not already existent) returning v$ to components using vuelidate')
    .option('--dollar', 'renames every "$v" identifier in js code to "v$". in template code, it does a regex replace with v$ to match "$v" in quotes or in quotes followed by a dot.')
    .action(params => {
        const opts = program.opts();
        opts.dryRun = params.dryRun || false;
        opts.useSetup = params.setup || false;
        opts.dollar = params.dollar || false;
        opts.install = params.install || false;
        optsForInstall(params, opts);

        if ((opts.npm || opts.yarn) && !opts.install) {
            console.error('error: passed yarn or npm option without install option');
            process.exit(1);
        }

        (new Vuelidate2ChangesCommand(opts.path, opts.dryRun, opts.useSetup, opts.dollar, opts.install, opts.yarn, opts.npm)).execute();
    });

program
    .command('add-emits')
    .option(...dryRunOptionArgs)
    .action((params) => {
        const opts = program.opts();
        opts.dryRun = params.dryRun || false;

        (new AddEmitsCommand(opts.path, opts.dryRun)).execute();
    });

program
    .command('misc-changes')
    .action(() => {
        const opts = program.opts();
        (new MiscChangesCommand(opts.path)).execute();
    });

if (!process.argv.slice(2).length) {
    showUsageError();
}

program.on('command:*', showUsageError);

program.parse(process.argv);
