module.exports = {
    root: true,
    parserOptions: {
        ecmaVersion: 2020
    },
    env: {
        node: true
    },
    extends: ['google'],
    rules: {
        'arrow-parens': 0,
        'comma-dangle': ['error', 'never'],
        'indent': ['error', 4],
        'max-len': 0,
        'import/extensions': 0,
        'import/no-unresolved': 0,
        'import/no-extraneous-dependencies': 0,
        'space-before-function-paren': ['error', 'always'],
        'space-in-parens': ['error', 'never'],
        'require-jsdoc': 0
    }
};
