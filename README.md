# q-migrate

Inofficial tool to help migrate a quasar v1 project to quasar v2 (alpha/beta).
Comes without any warranty.

**BACKUP YOUR PROJECT(S) BEFORE PROCEDING.**

It's not recommended to use this without eslint, since your markup's formatting could (partially) get changed.

## What & why

When updating your projects, you could rely on copy and paste and manually adjusting what went wrong. That is not only boring, but also dirty and a waste of time. This project uses `recast`* to read .js files, parse them, make changes and turn them back into code. `recast` tries to only modify the formatting of your code where changes occured.
Because we have access to the parsed nodes, we can easiliy see if we're modifying an Expression, Identifier and so on - less errors than doing a search and replace!

*\* I couldn't fully get it to work with .vue files and vue-eslint-parser yet, so the command(s) affecting .vue files use additional techniques that might have some side effects. Use `--dry-run` where possible, to get a preview of the changes.*

## Usage

`node q-migrate.js <command> -p <path-to-quasar-project>`

*command names are passed in lower case, as listed below*

## Commands (execute in order for least incomplete ;-) migration):

### stylus-to-scss (`src/commands/StylusToScssCommand`)

Will warn you about any stylus stylesheets. At this moment, it does *not* automatically convert them.

### install-v2 (`src/commands/InstallV2Command`)

Will install the quasar v2 packages using yarn or npm.

Options:
* `-y` or `--yarn`: use yarn instead of autodetect
* `-n` or `--npm`: use npm instead of autodetect
* `-c` or `--clean` to clean files/directories `node_modules/`, `.quasar/` `package-lock.json` and `yarn.lock`.

*Note: autodetect is the default behavior and will look for a `yarn.lock` file and use yarn if that exists. But it will not work with `-c` or `--clean`, since this deletes `yarn.lock` if it exists.*

### router-changes (`src/commands/RouterChangesCommand`)

Will
* modify the wildcard routes in `src/router/routes.js`
* add imports and change the instantiation of VueRouter to use the functional variant in `src/router/index.js`

Pass `-d` or `--dry-run` to just print the changes instead of saving them.

### vuex-changes (`src/commands/VuexChangesCommand`)

Will
* modify `src/store/index.js` to use `createStore`, add needed imports, remove old `Vue.use` and check for old `strict` property, which now gets set to `process.env.DEBUGGING` (if set to old `process.env.DEV`)

Pass `-d` or `--dry-run` to just print the changes instead of saving them.

### i18n-changes (`src/commands/I18nChangesCommand`)

Will
* modify `src/boot/i18n.js` to use `createI18n`, add needed imports, remove old `Vue.use` and transform `locale` and (if set) `fallbackLocale` from e. g. `en-us` to `en-US` style
* modify `src/i18n/index.js` to change the locale key names of the default export from e. g. `en-us` to `en-US` style
* modify `quasar.conf.js` to change the locale name of `framework.lang` from e. g. `en-us` to `en-US` style
* (only with `-i` or `--install` option) install the `vue-i18n@rc` package. You can use `--yarn` or `--npm` options, as in `install-v2` command.

Pass `-d` or `--dry-run` to just print the changes instead of saving them.

*Note: if `-i` or `--install` option was passed, `-d` or `--dry-run` will nevertheless install the package. This is handy if you forgot `--install` at first run (although multiple writes should in theory not make any difference)*

### vuelidate2-changes (`src/commands/Vuelidate2ChangesCommand`)

Will
* update `src/boot/vuelidate.js` (if found) to use `app.use(VuelidatePlugin)` and add/remove imports accordingly.
* scan the `src/` dir and subdirs for `.vue` files which have vuelidate imports and rewrite them for vuelidate2.
* (only with `-s` or  `--setup` option) add a `setup` method (if not existent) to the files which returns `useVuelidate()` as `v$` (not `$v`!). Also imports `useVuelidate`.
* (only with `--dollar` option [has no short version]) replace `$v` with `v$` in identifiers and the template (template variant uses a regex replace).
* (only with `-i` or `--install` option) install the `@vuelidate/core` and `@vuelidate/validators` packages. You can use `--yarn` or `--npm` options, as in `install-v2` command.

Pass `-d` or `--dry-run` to just print the changes instead of saving them.

*Note: As with some other commands, `-d` or `--dry-run` will still install packages, when `-i` or `--install` was passed*

### add-emits

Will scan all .vue files in `src/` for calls of `this.$emit('<event>')` and add `<event>` to the `emits` array.

Pass `-d` or `--dry-run` to just print the changes instead of saving them.

### misc-changes

Will scan all .vue files in `src/` and inform you about most of the quasar related (now) legacy attributes and elements. Example output:

```
Results for file src/layouts/layout.vue:

    element "q-drawer" uses old attribute "content-class" (new: "class")
```

## Contributing

It's much easier to develop the visitors if you use a tool like https://astexplorer.net/

Don't forget to fix eslint issues by running `npm run eslint-fix` before committing.
